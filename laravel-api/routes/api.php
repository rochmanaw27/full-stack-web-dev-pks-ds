<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/post/create', 'PostController@create');
Route::post('/post', 'PostController@store');
Route::get('/post', 'PostController@index');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{post_id}/edit', 'PostController@edit');
Route::put('/post/{post_id}', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');

Route::get('/role/create', 'RoleController@create');
Route::post('/role', 'RoleController@store');
Route::get('/role', 'RoleController@index');
Route::get('/role/{role_id}', 'RoleController@show');
Route::get('/role/{role_id}/edit', 'RoleController@edit');
Route::put('/role/{role_id}', 'RoleController@update');
Route::delete('/role/{role_id}', 'RoleController@destroy');

Route::get('/comment/create', 'CommentsController@create');
Route::post('/comment', 'CommentsController@store');
Route::get('/comment', 'CommentsController@index');
Route::get('/comment/{comment_id}', 'CommentsController@show');
Route::get('/comment/{comment_id}/edit', 'CommentsController@edit');
Route::put('/comment/{comment_id}', 'CommentsController@update');
Route::delete('/comment/{comment_id}', 'CommentsController@destroy');
