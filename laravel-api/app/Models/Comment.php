<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable = ["content", "post_id"];

    use HasFactory;
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $incrementing = false;

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }

    public function post(){
        return $this->hasMany('App\Post');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
