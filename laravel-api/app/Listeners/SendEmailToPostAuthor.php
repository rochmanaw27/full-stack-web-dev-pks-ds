<?php

namespace App\Listeners;

use App\Events\CommentStored;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToPostAuthor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        //
    }
}
