<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Role;

class RoleController extends Controller
{
    public function store(Request $request){
        $validated = $request->validate([
            'content' => 'required',
            'post_id' => 'required',
        ]);
        $role = new Role;
        $role->content = $request->content;
        $role->post_id = $request->post_id;
        $role->save();

        return response()->json([
            'success' => true,
            'message' => 'Save berhasil',
            'data'    => $role
        ], 200);
    }

    public function index(){
        $role = Role::All();
        return response()->json([
            'success' => true,
            'message' => 'List Data Blog',
            'data'    => $post
        ], 200);
    }

    public function show($id){
        $role = Role::find($id);
        return response()->json([
            'success' => true,
            'message' => 'Show Data Blog',
            'data'    => $role
        ], 200);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'content' => 'required',
            'post_id' => 'required',
        ]);
        $role = Role::find($id);
        $role->content = $request->content;
        $role->post_id = $request->post_id;
        $role->save();
        return response()->json([
            'success' => true,
            'message' => 'Edit Success',
            'data'    => $role
        ], 200);
    }

    public function destroy($id){
        $role = Role::find($id);
        $role->delete();
        return response()->json([
            'success' => true,
            'message' => 'Delete Success',
            'data'    => $role
        ], 200);
    }
}
