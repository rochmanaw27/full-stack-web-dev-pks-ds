<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Comment;
use App\Mail\PostAuthorMail;
use Illuminate\Support\Facades\Mail;

class CommentsController extends Controller
{
    public function store(Request $request){
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required',
        ]);
        $comment = new Comment;
        $comment->title = $request->title;
        $comment->description = $request->description;
        $comment->photo = $request->photo;
        $comment->save();
        Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        return response()->json([
            'success' => true,
            'message' => 'Save berhasil',
            'data'    => $comment
        ], 200);
    }

    public function index(){
        $comment = Comment::All();
        return response()->json([
            'success' => true,
            'message' => 'List Data Blog',
            'data'    => $post
        ], 200);
    }

    public function show($id){
        $comment = Comment::find($id);
        return response()->json([
            'success' => true,
            'message' => 'Show Data Blog',
            'data'    => $comment
        ], 200);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required',
        ]);
        $comment = Comment::find($id);
        $comment->title = $request->title;
        $comment->description = $request->description;
        $comment->photo = $request->photo;
        $comment->save();
        return response()->json([
            'success' => true,
            'message' => 'Edit Success',
            'data'    => $comment
        ], 200);
    }

    public function destroy($id){
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json([
            'success' => true,
            'message' => 'Delete Success',
            'data'    => $comment
        ], 200);
    }
}
