<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;

class PostController extends Controller
{
    public function store(Request $request){
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required',
        ]);
        $post = new Post;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->photo = $request->photo;
        $post->save();

        return response()->json([
            'success' => true,
            'message' => 'Save berhasil',
            'data'    => $post
        ], 200);
    }

    public function index(){
        $post = Post::All();
        return response()->json([
            'success' => true,
            'message' => 'List Data Blog',
            'data'    => $post
        ], 200);
    }

    public function show($id){
        $post = Post::find($id);
        return response()->json([
            'success' => true,
            'message' => 'Show Data Blog',
            'data'    => $post
        ], 200);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required',
        ]);
        $post = Post::find($id);
        if($post){
            $user = auth()->user();
            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user',
                    'data'    => $post
                ], 200);
            }
        }
        $post->update([
            'title' => $request->title,
            'description' => $request->description,
            'photo' => $request->photo
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Edit Success',
            'data'    => $post
        ], 200);
    }

    public function destroy($id){
        $post = Post::find($id);
        $post->delete();
        return response()->json([
            'success' => true,
            'message' => 'Delete Success',
            'data'    => $post
        ], 200);
    }
}
