<?php

class Hewan{
    public $nama="harimau";
    public $jenis = "Karnivora";


    public function __construct($nama, $jenis)
    {
        $this->nama = $nama;
        $this->jenis = $jenis;
        echo "ini adalah ".$this->nama." dia adalah jenis hewan: ".$this->jenis;
    }
    public function __destruct()
    {
        echo "objek ".$this->nama." dihapus";
    }




}
$tikus = new Hewan("Tikus","Omnivora");
echo "<br>";
$kucing = new Hewan("Kucing","Karnivora");
echo "<br>";
unset($tikus);
echo "<br>";
unset($kucing);

?>